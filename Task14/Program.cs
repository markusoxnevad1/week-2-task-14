﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_13
{
    class Palindrome
    {
        static void Main(string[] args)
        {
            Console.WriteLine(largestPalin());

        }
        //  Finds every palindrome consisting of two three digit numbers
        //  Then adds them to a list, and returns the largest number
        public static int largestPalin()
        {
            List<int> palinnumbers = new List<int>();
            for (int i = 999; i > 100; i--)
            {
                for (int j = 999; j > 100; j--)
                {
                    //  Checks if a number is a palindrome using the isPalin method
                    int mul = i * j;
                    if (isPalin(mul) == mul)
                    {
                        palinnumbers.Add(mul);
                    }
                }

            }
            return palinnumbers.Max();
        }
        //  Method that takes in a int and reverse it
        public static int isPalin(int number)
        {
            int Reverse = 0;
            while (number > 0)
            {
                int remainder = number % 10;
                Reverse = (Reverse * 10) + remainder;
                number = number / 10;
            }
            return Reverse;
        }
    }
}
